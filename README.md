# Deep Learning Computer Vision Assignment

In this assignment, we were tasked with creating a model for image recognition using the
xView database of objects. The xView database contains a large collection of satellite imagery,
annotated with the location and type of various objects, including buildings, vehicles, and
natural features. Our goal was to use this data to train a model that could accurately identify
different objects within an image.

The goals of the assignment include developing proficiency in using
Tensorflow/Keras for training Neural Nets (NNs), optimizing parameters and architecture of a
feed-forward Neural Net (ffNN), designing and optimizing the parameters of a Convolutional
Neural Net (CNN) for image recognition, training popular architectures from scratch and
comparing the results with their pre-trained versions using transfer learning. The xView database
is a large publicly available object detection dataset with approximately one million objects
across 60 categories, containing manually annotated images from different scenes around the
world. For this practice, we will discard most categories and only use the 12 most represented
classes, resulting in 717 and 79 images for training and testing respectively for the image
detection task. There is a severe class imbalance, and most annotations come from ”Building”
and ”Small car,” which affects the overall accuracy and should be considered when training
the model.
